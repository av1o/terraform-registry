package registry

import (
	"context"
	"github.com/hashicorp/terraform/registry/response"
)

type ProviderRegistry interface {
	ListAvailableVersions(ctx context.Context, namespace, pType string) (*response.TerraformProviderVersions, int, error)
	FindPackage(ctx context.Context, namespace, pType, version, os, arch string) (*response.TerraformProviderPlatformLocation, int, error)
	PublishPackage(ctx context.Context, namespace, pType, version string, opts *PublishOpts) error
}
